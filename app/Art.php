<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User;

class Art extends Model
{
    protected $table = 'art';

    protected $fillable = [
        'user_id',
        'title',
        'price',
        'image_url'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }
}
