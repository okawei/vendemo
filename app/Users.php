<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table = 'users';

    protected $fillable = [
        'email',
        'password',
        'name',
        'age'
    ];

    public function art(){
        return $this->hasMany(Art::class, 'user_id', 'id');
    }
}
