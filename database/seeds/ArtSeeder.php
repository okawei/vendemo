<?php

use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class ArtSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        foreach(User::all() as $user){
            for($x = 0; $x < rand(0, 4); $x++){
                \App\Art::create([
                    'user_id'=>$user->id,
                    'title'=>$faker->sentence,
                    'price'=>$faker->randomFloat(3),
                    'image_url'=>$faker->url
                ]);
            }
        }
    }
}
