<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Users::truncate();
        \App\Art::truncate();
        $this->call(UsersSeeder::class);
        $this->call(ArtSeeder::class);
        // $this->call(UsersTableSeeder::class);
    }
}
