<?php

use App\Users;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Auth;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        foreach(range(0, 100) as $x)
            Users::create([
                'email' => $faker->email,
                'name'=>$faker->name,
                'age'=>$faker->randomNumber(2),
                'password'=>\Illuminate\Support\Facades\Hash::make('secret')
            ]);
    }
}
